# if you don't want C++11 just define MARIADB_WITHOUT_CPP11 and boost library will be used instead

# Includes
ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif
IDIR        =include
IDIR_NATIVE?=../mariadb-native-client/include
IDIR_MYSQL = /usr/include/mysql
# Default folders
ODIR=obj
LDIR=lib
SDIR=src
OSDIR=$(ODIR)/$(SDIR)
DIRS=$(LDIR)	$(ODIR)	$(OSDIR)

# Sources
SRCS=account.cpp bind.cpp concurrency.cpp connection.cpp date_time.cpp decimal.cpp exceptions.cpp last_error.cpp private.cpp result_set.cpp save_point.cpp statement.cpp time.cpp time_span.cpp transaction.cpp worker.cpp

CXX     =clang++-3.9
AR      =ar -r -s
CPPFLAGS=-I$(IDIR) -I$(IDIR_NATIVE) -I$(IDIR_MYSQL) -std=c++11 -fPIC #-DMARIADB_WITHOUT_CPP11
CXXFLAGS=-Wall -O2
LIBS    =-lz -lssl -lstdc++ -pthread
TARGET  =libmariadb++.so
MKDIR   =mkdir -p

OBJS=$(patsubst %.cpp,$(OSDIR)/%.o,$(SRCS))
DEPS=$(patsubst %.cpp,$(OSDIR)/%.d,$(SRCS))

.PHONY:	clean all

all:	$(TARGET)

# Build target
$(TARGET):	makedirs	$(LDIR)	$(ODIR)	$(OSDIR)	$(OBJS)	$(DEPS)
	$(CXX) -shared -Wl,-soname,$(TARGET).1 -o $(TARGET).1.0.0 $(OBJS)

# Objects generation
$(OSDIR)/%.o:	$(SDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $< -o $@

# Generating dependencies
$(OSDIR)/%.d:	$(SDIR)/%.cpp
	$(CXX) -M $(CXXFLAGS) $(CPPFLAGS) $< > $@

#Cleanup objects / dependencies and target
clean:
	rm -f $(OBJS) $(DEPS) $(TARGET)

install: $(TARGET)
	cp -a  $(IDIR)/mariadb++ $(PREFIX)/include/
	install  $(TARGET).1.0.0 $(PREFIX)/lib/
	ln -fs $(PREFIX)/lib/$(TARGET).1.0.0 $(PREFIX)/lib/$(TARGET).1
	ln -fs $(PREFIX)/lib/$(TARGET).1 $(PREFIX)/lib/$(TARGET)

# Create directories
makedirs:
	@$(call make-dirs)

define make-dirs
	for dir in $(DIRS);\
	do\
		mkdir -p $$dir;\
	done
endef
